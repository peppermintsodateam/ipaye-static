module.exports = function(grunt) {
	grunt.initConfig({

		less:{
			dev:{
				options:{
					paths: ["./less"],
                    yuicompress: true

				},

				files: {
      				"src/css/main.css": "src/less/style.less"
    			}
			}
		},

		cssmin: {
			prod:{
				files: {
					"build/css/main.css" : "src/css/main.css",
					"build/css/screen.css" : "src/css/screen.css",
					"build/css/magnific-popup.css" : "src/css/magnific-popup.css"
				}
			}
		},


		uglify: {

			prod:{
				options:{
					mangle:false//aby zachowac nazwy zmiennych
				},

				files:{
					"build/js/main.js": "src/js/main.js",
					"build/js/vendor/circle-canvas.js": "src/js/vendor/circle-canvas.js",
					"build/js/vendor/google-map.js": "src/js/vendor/google-map.js",
					"build/js/vendor/ajax-script.js": "src/js/vendor/ajax-script.js",
					"build/js/vendor/jquery.bez.js": "src/js/vendor/jquery.bez.js"

				}
			}	
		},


		autoprefixer: {

			dev: {
				options: {
					browsers:["last 25 version"]
				},

				src:"src/css/*.css"
			}

		},


		concat_css: {
		    options: {
		      // Task-specific options go here. 
		    },

		    prod: {
		      src: ["src/css/owl.theme.default.min.css", "src/css/owl.carousel.min.css","src/css/magnific-popup.css"],
		      dest: "src/css/screen.css"
		    },
  		},

		watch:{
			options:{
				livereload:true
			},

			dev:{
				files:["src/**/*"],
				tasks:["dev"] 
			}
		}


	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-concat-css');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.registerTask("dev",["less","autoprefixer"]);
	grunt.registerTask("prod",["uglify","cssmin","concat_css"]);//server produkcyjny
	grunt.registerTask("default","dev");//po wpisaniu grunt, wykonaja si tylko wersja developerska
	grunt.registerTask("build",["dev","prod"]);


}










