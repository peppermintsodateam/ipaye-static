(function ($) {

// VARIABLES
    var mainContent = $('.main-slides-wrapper');
    var slidesContainer = mainContent.find('.slides-container');
    var slides = slidesContainer.children();
    var slidesWidth = slides.width($(window).width());
    var slidesCount = slides.length;
    var currentSlideID = 0;
    var sections = $('.section, .main-slide-content');
    var curSlide = slides.first();
    var linksPag = $('.pagination-list li').find('span');
    var loadedPage = true; // After page is loaded is zero
    var isAnimating = false;
    var refresh = false;
    var wasUsedBackButton = false;
    var slideIntervalId = 0;
    var leftNavigation = $('.left-navigation');
    var doc = $(document);
    var box = $('.tile');
    var browserwidth = $(window).width();
    var tilesWrapper = $('.main-wrapper').find('.tiles-wrapper');
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ? true : false;

    var tl = new TimelineMax();
    var resizeTimer;

    var caseItems = $('.topics');
    var mainTopicContainer = $('.content-inner');
    var maxSections = caseItems.length;
    var caseNext = $('.arrows-wrapper').find('a.next');
    var casePrev = $('.arrows-wrapper').find('a.prev');

    var currentCaseID = 0;
    var loadedPage = false;
    var zIndex = 10;

    var mainSlider = $('.testimonial-grid-rotator');
    var slidesWrapper = mainSlider.find('.slides-wrapper');
    var slidesEl  = slidesWrapper.children();
    var slidesCounter = slidesEl.length;
    var singleSlide = $('.slide');
    var paginationList = $('.pagination-list-testimonials');
    var testimonailPag = $('.pagination-list-testimonials li');
    var slideCount = 0;
    var currentTestimonialSlide = 0;
    var testimonialIntervalId = 0;
    var singleHeader = $('.sect-masthead').find('.masthead-content');

    function homeSlideMenu() {
        var classes = ['uk-ico','eu-ico','rest-ico'];
        var counter = 0;
        var slideLinks = $('.countries-choose li').find('a');

            slideLinks.each(function(index,item){

            if(counter%classes.length === 0) {
                //Co 3 elementy przeskakuje na poczatek tablicy
                counter = 0;
            }

            $(item).addClass(classes[counter]);
            counter++;
        });
    }



    function agencyTabs() {
        var tabsNumber = 0;
        var tabsNav = $('#filter-list').find('li');

        tabsNav.each(function(){

            var that = $(this);
            that.attr('data-tabs-content',tabsNumber + 1);
            tabsNumber ++;
        });

        tabsNav.filter(':first').addClass('active-tab');

        var content = $('.tabs').find('.tabs-content');
        content.not(':first').hide();

        content.each(function(i){
            var that = $(this);
            that.attr('data-tabs-content',(i+1));
            
        });

        tabsNav.on('click', function(){
            
            var that = $(this);
            if($(that).hasClass('active-tab')) {
                return;
            }

            var targetData = that.data('tabs-content');
            var target = $('.tabs').find('.tabs-content').filter(function(){
                return $(this).data('tabs-content') === targetData;
            });

            $('#filter-list li').removeClass('active-tab');
             that.addClass('active-tab');

            content.hide();
            target.fadeIn(400);
        });

    }


    function ukTabs() {
        var tabsNumber = 0;
        var tabsNav = $('.list-working-uk').find('li');
        //tabs-content-uk

        tabsNav.each(function(){

            var that = $(this);
            that.attr('data-tabs-uk',tabsNumber + 1);
            tabsNumber ++;
        });

        tabsNav.filter(':first').addClass('active-tab');

        var content = $('.tabs-uk').find('.tabs-content-uk');
        content.not(':first').hide();

        content.each(function(i){
            var that = $(this);
            that.attr('data-tabs-uk',(i+1));
            
        });

        tabsNav.on('click', function(){
            
            var that = $(this);
            if($(that).hasClass('active-tab')) {
                return;
            }

            var targetData = that.data('tabs-uk');
            var target = $('.tabs-uk').find('.tabs-content-uk').filter(function(){
                return $(this).data('tabs-uk') === targetData;
            });


            $('.list-working-uk li').removeClass('active-tab');
             that.addClass('active-tab');

            content.hide();
            target.fadeIn(400);
        });
    }


    function euTabs() {
        var tabsNumber = 0;
        var tabsNav = $('.list-working-eu').find('li');
        //tabs-content-eu

        tabsNav.each(function(){

            var that = $(this);
            that.attr('data-tabs-eu',tabsNumber + 1);
            tabsNumber ++;
        });

        tabsNav.filter(':first').addClass('active-tab');

        var content = $('.tabs-eu').find('.tabs-content-eu');
        content.not(':first').hide();

        content.each(function(i){
            var that = $(this);
            that.attr('data-tabs-eu',(i+1));
            
        });

        tabsNav.on('click', function(){
            
            var that = $(this);
            if($(that).hasClass('active-tab')) {
                return;
            }

            var targetData = that.data('tabs-eu');
            var target = $('.tabs-eu').find('.tabs-content-eu').filter(function(){
                return $(this).data('tabs-eu') === targetData;
            });


            $('.list-working-eu li').removeClass('active-tab');
             that.addClass('active-tab');

            content.hide();
            target.fadeIn(400);
        });
    }

    ukTabs();
    euTabs();

    function slideToDownload() {
        $('.download-button').on('click', function(e){
            e.preventDefault();

            var that = $(this);
            var targetData = that.data('download-section');
            var target = $('#download-section').filter(function(){
                return $(this).data('download-section') === targetData;
            });

            var offset = Math.floor(target.offset().top) - 50;
           
           $('html, body').stop(true).animate({
                scrollTop:offset
           },600);

        });
    }

    function filterDownload() {
        $('#category-filter-section').mixItUp({

            load: {
                sort: 'order:asc'
            },

            animation: {
                effectsIn: 'fade translateY(-100%)',
                duration: 700 /* 600 */
            },

            selectors: {
                target: '.mix',
                filter: '.filter-category',
                sort: '.sort-btn'
            },

             callbacks: {
                onMixEnd: function(state){
                   // resizePost();
                }
             }

          });

        var filterTabs = $('.category-filter-list').find('li');

        filterTabs.on('click', function(){
            $('.category-filter-list li').removeClass('all-btn');
        })
    }

    filterDownload();

    function downloadPlugin() {
        $('.downloads-inner').find('.sdm_post_title').remove();
        $('.sdm_download_link').find('form').addClass('downloadForm');
        var passwordField = $( "[type=password]" );
        var formPassword = $('.downloadForm').find(passwordField);
        formPassword.addClass('inputPassword');

        var downloadBtn = $('.sdm_download_link').find('a');
        downloadBtn.text("Download");
        downloadBtn.attr('target','_blank');

        /*if ($('.inputPassword').val().length === 0) {
            $('.pass_sumbit').hide();
        } else if($('.inputPassword').val().length === 17) {
            $('.pass_sumbit').show();
        }*/

        $('.pass_sumbit').hide();

        $('.inputPassword').on('blur', function(){
            var that = $(this);
            if(that.val().length < 17 ) {
                that.siblings('.pass_sumbit').hide();
            //$('.pass_sumbit').hide();
        }else if ( $(this).val().length >=17 ) {
            //$('.pass_sumbit').show();
            that.siblings('.pass_sumbit').show();
        }
    });


       $('.inputPassword').on('paste', function(){
            var that = $(this);
            that.siblings('.pass_sumbit').show();
            /*var that = $(this);
            if( $(this).val().length < 17 ) {
                that.siblings('.pass_sumbit').hide();
            }else if ( $(this).val().length >= 17 ) {
                that.siblings('.pass_sumbit').show();
        }*/

        });




        $('.inputPassword').on('keypress', function(){
            var that = $(this);
            if($(this).val().length >= 17){
                 that.siblings('.pass_sumbit').show();
                return false;
            }else if ($(this).val().length < 17 ) {
                that.siblings('.pass_sumbit').hide();
            }
        });
    }

    downloadPlugin();

    function slideDescriptionClass() {
        var counter = 0;
        var description = $('.countries-choose li').find('.country-desc');
        var descClass = ['uk','eu','rw'];

             description.each(function(index,item){
                $(item).addClass(descClass[counter]);
                counter++;
            });

        $('.country-desc.uk').text('United Kingdom');
        $('.country-desc.eu').text('European Economic Area');
        $('.country-desc.rw').text('Elsewhere');
    }

    homeSlideMenu();
    slideDescriptionClass();

    // Functionality for testimonial slider
    function goToNextTestimonial() {
        var testimonialGo = currentTestimonialSlide + 1;

        if(testimonialGo >= slidesCounter) {
            testimonialGo = 0;
        }

        gotoTestimonial(testimonialGo);
    }


    function addSpantoBtn() {
        var buttonNext = $('.page-bottom-area').find('.next-article');
        var buttonPrev = $('.page-bottom-area').find('.prev-article');

        buttonNext.append('<span class="inside-btn right-arrow-btn"></span>');
        buttonPrev.append('<span class="inside-btn left-arrow-btn"></span>');
    }


    // Testimonials slide interval
    function startAutoPlayTestimonials(immiediatestart) {
        //Przerywam interwal i rozpoczynam nowy
        clearInterval(testimonialIntervalId);
        testimonialIntervalId = setInterval(goToNextTestimonial,5000);
        if(immiediatestart) {
            goToNextTestimonial(); 
        }
    }


     function gotoTestimonial(testimonialID) {
        // Zapamietuje ostatnio wybrany element
        currentTestimonialSlide = testimonialID;

         $('.pagination-list-testimonials li').eq(testimonialID).click();
    }


    function createPagination() {
        slidesEl.each(function(){
            $(this).attr('data-slide', slideCount);
            slideCount = slideCount + 1;
        });


        //Creating pagination
        if(!paginationList.length) {
            paginationList = $('<ul></ul', {
                'id':'pagination-list-testimonials',
                'class':'pagination-list-testimonials'
            }).appendTo(mainSlider);
        }
    }


    function testimonialWrapperWidth() {
        // Dynamic width for slidesWrapper
        slidesWrapper.css({

            'width':(slidesCounter * 100 + '%'),
            'margin-left':'0%'

        });
    }

    function slideTestimonialWidth() {
         //Dynamic width for each slide
        singleSlide.css({

            'width': ((100/slidesCounter)+'%')
            //'width':$('.viewport').width()

        });
    }

    //sections.width($(window).width());

    function paginationSlidesLength() {

        //Show pagination according to slides length
        //paginationList.empty();

        var bulletCount = 0;
        for(var i=0; i<slidesCounter; i++) {

             var paginationEl = $('<li></li>', {

            }).appendTo(paginationList);

            var pagBullet = $('<span></span>', {
                'class':'bullet'
            }).appendTo(paginationEl);

            

            pagBullet.each(function(){
                $(this).addClass('bulletnumber' + bulletCount);
                $(this).attr('data-slide', bulletCount);
                bulletCount = bulletCount + 1;
            });
            
        }
    }


    function paginationClick() {
        var pagClick = paginationList.find('li');

        pagClick.filter(':first').addClass('activePag');

        pagClick.on('click',function(){

            paginationList.find('li').removeClass('activePag');
            var that = $(this);
            that.addClass('activePag');

            var targetId = that.children('.bullet').data('slide');

            var target  = $('.slides-wrapper').find('.slide').filter(function() {
                return $(this).data('slide') === targetId;
            });

            currentTestimonialSlide = target.data('slide');
            var targetWidth = target.outerWidth();
            var targetIndex = target.index();
            var targetMargin = -(targetIndex * targetWidth);

            slidesWrapper.stop(true).animate({
                'margin-left': targetMargin
            },

                {
                    duration: 200
                }

            );

        });
    }


    // Case studies next and preview

    caseNext.on('click',function(e){
        e.preventDefault();

        var slideToGo = currentCaseID + 1;
        if (slideToGo >= maxSections) {
            slideToGo = 0;
        }

        gotoCase(slideToGo);
        
    });

    casePrev.on('click', function(e){
        e.preventDefault();
        var slideToGo = currentCaseID - 1;

        if (slideToGo < 0) {
            slideToGo = maxSections - 1;
        }

        gotoCase(slideToGo);

    });

    function gotoCase(slideToGo) {
        //caseItems.removeClass('active-section');
        var currentCaseContent = caseItems.find('.topic-content-wrapper');
        var activeTopicContent = $('.active-section').find('.topic-content-wrapper');
        //caseItems.hide();

        TweenMax.to(currentCaseContent, 0.2,
            {
                opacity:0,
                ease: Power4.easeOut,
                onComplete:function() {
                    TweenMax.to(caseItems, 0.2, 

                        {
                            className:"-=active-section",
                            ease:Power0.easeNone,
                            onComplete:function() {
                                currentCaseID = slideToGo;
                                caseItems.eq(currentCaseID).addClass('active-section');
                                $('.active-section').find('.topic-content-wrapper').css({
                                    opacity:1,
                                    visibility:"visible"
                                })
                            }
                        }

                    )
                }
            }
        )

        // Zapamietuje ostatnio wybrany element
        currentCaseID = slideToGo;

        caseItems.eq(currentCaseID).addClass('active-section');
        //$('.active-section').fadeIn(600);
    }



    // Tiles grid function
    function onResize() {
        var front = $('.front');
        var back = $('.back');
        var contentInner = $('.content-inner');
        var blogPost = $('.blog-post');

        var frontFeature = $('.front-feature');
        var backFeature = $('.back-feature');

        frontFeature.css('height', $('.tile').height() - 3);
        backFeature.css('height', $('.tile').height() - 3);
        frontFeature.css('width', $('.tile').width() - 3);
        backFeature.css('width', $('.tile').width() - 3);

        if($(window).width() > 1240) {
            front.css('height', $('.tile').height() - 3);
            back.css('height', $('.tile').height() - 3);
            front.css('width', $('.tile').width() - 3);
            back.css('width', $('.tile').width() - 3);
        } else {
            front.css('height', $('.tile').height());
            back.css('height', $('.tile').height());
            front.css('width', $('.tile').width());
            back.css('width', $('.tile').width());
        }

        $('.front-shape').css('height', $('.logo-col').height());
        $('.back-shape').css('height', $('.logo-col').height());
        $('.front-shape').css('width', $('.logo-col').width());
        $('.back-shape').css('width', $('.logo-col').width());

        $('.desc-col').css('height',$('.logo-col').height());

        /*$('.blog-post-content').css({
            'height': blogPost.width()
        });

        $('.blog-post-content').css({
            'width': blogPost.width()
        });*/

        // Header articles settings
        var featuresHeader = $('.main-features');
        

        if($(window).width() > 1240) {
            var featuresHeaderWidth = Math.floor((featuresHeader).width() / 4);
            //console.log(featuresHeaderWidth);

        } else if ($(window).width() > 992) {
            var featuresHeaderWidth = Math.floor((featuresHeader).width() / 3);
           // console.log(featuresHeaderWidth);
        }


        $('.js-height').height($(window).height() - 500);

        
    }


    function closeTeamPanels() {
        var allBios = $('.company-bio').find('.bio-description');
        
        /*window.location.hash = ""*/

        // Reset browsing history and get rids of "#"
        //history.replaceState({}, document.title, location.href.substr(0, location.href.length-location.hash.length)); 
        //window.history.back(-1);

        if(window.history.pushState) {
            window.history.pushState('', '/', window.location.pathname)

        } else {
            window.location.hash = '';
        }

         // Stop animation on scroll 'jump' avoiding
            var bodyPage = $('html, body');

            bodyPage.on("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove", function(){
               bodyPage.stop();
            });

             bodyPage.animate({ scrollTop: 0 }, 'slow', function(){
                bodyPage.off("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove");
            });

        //window.history.back(1);*/

        allBios.stop(true,true).slideUp(400, $.bez([0.165, 0.84, 0.44, 1]));
    }


    function scrollToTeam() {

        var allBios = $('.company-bio').find('.bio-description');
        allBios.hide();
        var collapseAll = true;

        $('.team-member').on('click', function(){

            var that = $(this);
            var targetId = that.data('caseLink');

             if(collapseAll) {
                allBios.stop(true).slideUp(400, $.bez([0.165, 0.84, 0.44, 1]));
            }

            if(!targetId) return;
            var target = $('.company-bio').find('.bio-description').filter(function(){
                return $(this).data('caseLink') === targetId;
            });

            target.addClass('active-bio');

            var offset = Math.floor($('.company-bio').offset().top - $('.home-header').outerHeight());
            var isVisible = target.is(':visible');

             if(!isVisible) {
                target.stop(true).slideDown(400, $.bez([0.165, 0.84, 0.44, 1]));
            } else {
                 target.stop(true).slideUp(400, $.bez([0.165, 0.84, 0.44, 1]));
            }

            // Stop animation on scroll 'jump' avoiding
            var page = $('html, body');

            page.on("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove", function(){
                page.stop();
            });

             page.animate({ scrollTop: offset }, 'slow', function(){
                page.off("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove");
            });


            if(typeof history.pushState !== "undefined") {
                var id = target.data('caseLink');
                history.pushState({id:id}, '', '#' + id);
            }
        });

        $('.close-panel-team').on('click',closeTeamPanels);
    }


   


    // Function case studies open panel
    function panelCaseAnimation() {
        var contentInner = $('.topic-content-wrapper');

        TweenMax.to(contentInner, 0.5,
            {
                opacity:1,
                visibility:"visible",
                ease: Power4. easeOut
            }
        )
    }

    function removeShapes() {
        $('.shape-list').hide();                    
    }

    function fadeInGrid() {

        
        TweenMax.to($('.tiles-wrapper'), 1.0,

            {
                opacity:1,
                visibility:"visible",
                ease:Power4.easeNone

            }

        )
    }

    function showGridBack() {
         $('.shape-list').show();
         var contentInner = $('.topic-content-wrapper');
         var hexItems = $('.shape-list').find('.i-paye-study');
         var activeSection = $('.content-inner').find('.active-section');

         if (activeSection.length !== 0) {
            activeSection.removeClass('active-section');
            //activeSection.hide();
         }

         TweenMax.to(contentInner, 0.5,

            {
               autoAlpha:0,
                ease: Power4.easeOut,
                onComplete:function() {
                    fadeInGrid();
                }
            }
        )

        $('.content-inner').fadeOut('200');
    }

    function caseStudyOpen() {

        var hexItems = $('.shape-list').find('.i-paye-study');
        var mainTopicContainer = $('.content-inner');
        var contentInner = $('.topic-content-wrapper');
        var texts = mainTopicContainer.find('.topics');
        //texts.hide();


        var closePanelButton = $('.close-panel');

        if($('.i-paye-study'!==0)) {

            $('.i-paye-study').on('click',function(){

                $('.content-inner').show();
                $('html').addClass('no-scroll');


                var that = $(this);
                
                var targetId = that.data('caseLink');
                var target = $('.content-inner').find('.topics').filter(function(){

                    return $(this).data('caseLink') === targetId;

                });

                target.addClass('active-section');

                // Musi byc rowniez oznaczony index
                currentCaseID = target.index();
                var currentClickedTopic = $('.active-section').find('.topic-content-wrapper');

                TweenMax.to(currentClickedTopic, 0.5,
                    {
                        opacity:1,
                        visibility:"visible",
                        ease: Power4.easeOut
                    }
                )
            });
        }



        closePanelButton.off().on('click', function(){
            showGridBack();
             $('html').removeClass('no-scroll');
        });
    }

    caseStudyOpen();


    function desktopHover() {
        if('.shape-list'!==0) {
            var backFace = $('.shape-list .i-paye-study,.shape-list .team-member').find('.backface');

            backFace.on('mouseenter', function(){
                var that = $(this);

                that.addClass('change-fade');
                

            }).on('mouseleave', function(){
                 var that = $(this);
                that.removeClass('change-fade');
                
            });
        }
    }

    

    function dropDownNavigation() {
        var dropDownLink = $('.main-navigation-list .drop-down-anchor > a');
        var defaultSub = $('.main-navigation-list .drop-down-anchor').find('.sub-menu');

        defaultSub.addClass("sub-nav-list");

        var dropIcon = $('.drop-nav-icon');

        if(!dropIcon.length) {
            dropIcon = $('<span></span>', {
                'class': 'drop-nav-icon'
            });
        }

        dropIcon.appendTo(dropDownLink);

        var clickedIcon = $('.main-navigation-list .drop-down-anchor > a').find('.drop-nav-icon');
        var subNav = $('.main-navigation-list .drop-down-anchor').find('.sub-nav-list');
        subNav.hide();

        var allCollapsed = false;

        clickedIcon.on('click', function(e){

            if(!allCollapsed) {
                subNav.stop(true).slideUp(250, $.bez([0.165, 0.84, 0.44, 1]));
            }

            e.preventDefault();
            var that = $(this);
            var matchedNavigations = that.closest('.drop-down-anchor').children('.sub-nav-list');
            var isVisible = matchedNavigations.is(':visible');

            if(!isVisible) {
                matchedNavigations.stop(true,true).slideDown(250, $.bez([0.165, 0.84, 0.44, 1]));
            } else {
                matchedNavigations.stop(true,true).slideUp(250, $.bez([0.165, 0.84, 0.44, 1]));
            }

        });
    }

    function paddingPanel() {
        var panelContent = $('.panel-content');
        var headerHeight = $('.home-header').outerHeight();
        var liveChatHeight = $('.live-chat-wrapper').outerHeight();
        var chatHeight = $('.panel-content-inside').outerHeight() - headerHeight - liveChatHeight - $('.bck-bottom').outerHeight() - 280;
        var footerHeight = $('.footer-grid').outerHeight();

        panelContent.css({

            'padding-bottom': chatHeight

        });
    }


    function flipTiles() {
        TweenMax.set(".flip-container", 
            {
                perspective:1000
            }

        );

            TweenMax.set(".flipper", 
                {
                    transformStyle:"preserve-3d"
                }

            );

            TweenMax.set(".back", {rotationY: 180});
            TweenMax.set([".back", ".front"], {

                transformStyle: "preserve-3d",
                backfaceVisibility:"hidden"
            });


            TweenMax.set(".back-feature", {rotationY: 180});
            TweenMax.set([".back-feature", ".front-feature"], {

                transformStyle: "preserve-3d",
                backfaceVisibility:"hidden"
            });



        TweenMax.staggerTo($(".flipper"), 0.4, {rotationY: 360, autoAlpha:1, repeat:0, yoyo:true}, 0.3);
    }


    function flipPageTitle() {
        TweenMax.set(".flip-container-shape", 
            {
                perspective:1000
            }
        );

        TweenMax.set(".flipper-shape", 
            {
                transformStyle:"preserve-3d"
            }

        );


            TweenMax.set(".back-shape ", {rotationY: 180});
            TweenMax.set([".back-shape", ".front-shape "], {

            transformStyle: "preserve-3d",
            backfaceVisibility:"hidden"
        }

        );

        TweenMax.staggerTo($(".flipper-shape"), 0.4, {rotationY: 360, autoAlpha:1, repeat:0, yoyo:true}, 0.3);
    }


    function hoverTiles() {
        TweenMax.set(".flip-container", 
            {
                perspective:1000
            }
        );

        TweenMax.set(".flipper", 
            {
                transformStyle:"preserve-3d"
            }
        );

        TweenMax.set(".back", {rotationY: 180});
        TweenMax.set([".back", ".front"], {
            transformStyle: "preserve-3d",
            backfaceVisibility:"hidden"
        });


        TweenMax.set(".back-feature", {rotationY: 180});
        TweenMax.set([".back-feature", ".front-feature"], {
            transformStyle: "preserve-3d",
            backfaceVisibility:"hidden"
        });

        if($(window).width() > 1025) {
            $('.container-hover').hover(

                function(){
                    TweenMax.to($(this).find(".flipper"), 1.2, {rotationY:180, ease:Back.easeOut});
                },

                function() {
                    TweenMax.to($(this).find(".flipper"), 1.2, {rotationY:0, ease:Back.easeOut});
                }

            );
        }else {
            return;
        }
    }


    /*Height section plugin*/

    $.fn.setSectionHeight = function () {
        return this.css({
            'height':$(window).height()
        });
    }

    /*Set min height for tiles container*/
    $.fn.setMinHeight = function() {
        return this.css('min-height', $(window).height());
    }

    // Function which adds extra class on scroll to main home slider

    function addClassonScroll() {
        if($(window).width() <= 1025) {

            if($('.slides-container').length > 0) {
                var offset = $('.slides-container').offset().top;
                var winPos = $(window).scrollTop();

                if(Math.floor(offset) - winPos - 30 <= 0) {
                    $('.slides-container').addClass('animate-color');
                    $('#switcher').addClass('switcher-white');
                }else {
                    $('.slides-container').removeClass('animate-color');
                    $('#switcher').removeClass('switcher-white');
                }
            }

        } else {
            $('.slides-container').removeClass('animate-color');
        }
    }

    addClassonScroll();

    // Slider code goes here
    function containerWidth() {
        slidesContainer.css({
            'width': (slidesCount * 100 + "%")
        });

        sections.width($(window).width());
    }


     function gotoNextSlide() {

        var slideToGo = currentSlideID + 1;
        if (slideToGo >= slidesCount) {
            slideToGo = 0;
        }
        gotoSlide(slideToGo,false);
    }

    function startAutoPlay(immiediate) {
        //Przerywam interwal i rozpoczynam nowy
        clearInterval(slideIntervalId);
        slideIntervalId = setInterval(gotoNextSlide, 80000000000000);
        if(immiediate) {
            gotoNextSlide(); 
        }
    }

    function stoponHover() {
            sections.on('mouseover',function(){
                clearInterval(slideIntervalId);
                
            }).on('mouseout',function(){
                startAutoPlay(false);
            });

            linksPag.on('mouseover',function(){
                clearInterval(slideIntervalId);

            }).on('mouseout',function(){
                startAutoPlay(false);
            });
        }

        stoponHover();


    function gotoSlide(slideID, loadedPage) {
        loadedPage = loadedPage;
        currentSlideID = slideID;
        linksPag.eq(slideID).click();
    }


    scrollToSection = function (e) {

        e.preventDefault();
        var that = $(this);
        var targetId = that.data('slider');
        /*console.log('targetID', targetId);*/
        if (!targetId) return;
        
        $('.pagination-list').find('span').removeClass('active-link');
        that.addClass('active-link');

        var target = $('.slides-container').find('section').filter(function () {
            // Pod target jest sekcja, ktora "maczuje" klikniety przycisk paginacji z sekcja
            return $(this).data('slider') === targetId;
        });


        currentSlideID = target.data('id');
        var targetWidth = target.outerWidth();
        var targetIndex = target.index();
        var targetMargin = -(targetIndex * targetWidth);
        //var leftContent = target.find('.slider-column-left');

  
        if (!refresh) {
            var time = (loadedPage) ? 1 : 0.4; // Po zaladowaniu strony animacja przesuniecia do slajdu
            loadedPage = false;

            slides.removeClass('activeAnimation');

            TweenMax.to($('.slides-container'), time,
                {
                    marginLeft: targetMargin,

                    onComplete: function () {
                        isAnimating = false;
                        var currentContainer = slides.eq(currentSlideID);
                        var prevContainer = slides.index();


                        currentContainer.addClass('activeAnimation');

                        // Animation after slide change
                        TweenMax.to(currentContainer.find('.slider-column-left'),0.8, {
                                opacity:1,
                                ease: Power4. easeOut,
                                x:0
                        });

                         TweenMax.to(currentContainer.find('.slider-column-right'),0.8, {
                                opacity:1,
                                ease: Power4. easeOut,
                                x:0,
                                delay:0.4
                        });
                        
                        
                    },ease: Power4. easeOut
                }
            )

        } else {
            $('.slides-container').css('margin-left', targetMargin);
        }

        for(var i = 0; i < slides.length; i++) {
            // Kazdy inny slajd od tego wyswietlanego obecnie bedzie mial opacity 0
            if(i!==currentSlideID) {
                var container = slides.eq(i).find('.slider-column-left');
                var containerRight = slides.eq(i).find('.slider-column-right');
                TweenMax.killTweensOf(container);

                container.css({
                    opacity:0,
                    "-webkit-transform":"translate(15px,0px)",
                    "-ms-transform":"translate(15px,0px)",
                    "transform":"translate(15px,0px)"
                });

                 containerRight.css({
                    opacity:0,
                    "-webkit-transform":"translate(15px,0px)",
                    "-ms-transform":"translate(15px,0px)",
                    "transform":"translate(15px,0px)"
                });
            }
        }

        refresh = false;

        // backButton - dodaj urla do historii przegladarki tylko jesli nie korzystasz z przyciskow przegladarki back lub next
        if (typeof history.pushState !== "undefined" && !wasUsedBackButton) {
            // Zmiana url w pasku przegladarki
            var id = target.data('slider');
            history.pushState({id: id}, '', "#" + id);
            /*console.log('id', id);*/
        }

        wasUsedBackButton = false;

    };


   function findHash() {

        // Replace usuwa has z data 
        var hash = (window.location.hash).replace('#', "");
        $(linksPag).filter(function () {
            return $(this).data('slider') === hash;
            // Trigger click robi klika na paginacji i przesuwa po załadowaniu strony ze sliderem do odpowiedniego slajdu
        }).trigger("click");

        // jesli nie ma wybranego hasha to zrob click na pierwszym elemencie
        if (hash === '') {
            if(linksPag.length!==0) {
                $(linksPag)[0].click();
            }
        }
    }


    jQuery.browser = {};
    (function () {
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
    })();

    

    doc.on("keyup", function(e){
        if(e.which===27) {
            $('#switcher').removeClass('open-nav');
            leftNavigation.removeClass('display-nav');
           // tilesWrapper.removeClass('change-index');
        }
    });


    function clickSwitcher() {
        var isOpen = false;

        $('#switcher').on('click', function () {
            var that = $(this);
            //that.addClass('open-nav');



            if(!isOpen) {
                that.addClass('open-nav');
                isOpen = true;
                leftNavigation.addClass('display-nav');
                //tilesWrapper.addClass('change-index');
                $('.home-header').addClass('header-index');

                if($(window).width() < 992) {
                    $('html, body').addClass('overflow-hidden');
                }

            } else {
                that.removeClass('open-nav');
                isOpen = false;
                leftNavigation.removeClass('display-nav');
               // tilesWrapper.removeClass('change-index');
                $('.home-header').removeClass('header-index');

                if($(window).width() < 992) {
                    $('html, body').removeClass('overflow-hidden');
                }
            }
        });
    }



    function playHexagonAnimation() {
        var hexItems = $('.shape-list').find('.i-paye-study');
        var teamMembers = $('.shape-list').find('.team-member');

        tl.staggerFromTo(hexItems, 1.5,

            {
                opacity:0,
                delay:0.8,
                x:-15

            },

            {
                opacity:1,
                ease: Power4. easeOut,
                force3D:true,
                x:0
            },0.2
        )

        tl.staggerFromTo(teamMembers, 1.5,

            {
                opacity:0,
                delay:0.8,
                x:-15

            },

            {
                opacity:1,
                ease: Power4. easeOut,
                force3D:true,
                x:0

            },0.2
        )
    }

    
    function homeSlider() {



        $('.home-rotator.owl-carousel').owlCarousel({
                items:1,
                loop:true,
                nav:false,
                dots:true,
                autoplay:true,
                autoplayTimeout:6000,
                mouseDrag:false,
                freeDrag:false,
                autoplayHoverPause: true
            });
    }



function swapHomeBtn() {
    var header = $('.home-header');
    var backLink = header.find('.back-home');

    backLink.detach().prependTo(header);

}


function personGridRotator() {

    var textInside = $('.text-item').find('.header-tile');
    var imageThumb = $('.thumbnail-rotator').find('.image-item');
    var carouselGrid = $('.grid-carousel.owl.carousel');
    var textInner = $('.owl-item.active .text-item').find('.header-tile');

    

    $('.grid-carousel').each(function(){
        var sync = $(this);


       /*
         sync.on('initialize.owl.carousel', function(event){

           TweenMax.to(textInside, 2.0, 
                {
                    autoAlpha:1,
                    ease: Power4. easeOut
                }
            )


            TweenMax.to(imageThumb, 1.5, 
                {
                    autoAlpha:1,
                    transformOrigin:"50% 50%",
                    scale:1,
                    ease: Power2.easeOut,
                    rotation:0,
                    scale:1
                }
            )

        });
       */



        sync.owlCarousel({
                items:1,
                loop:true,
                nav:false,
                dots:false,
                autoplay:true,
                autoplayTimeout:4500,
                mouseDrag:false,
                freeDrag:false,
                touchDrag : false,
                animateOut: 'fadeOut',
                animateIn: 'fadeIn'
            });


            /*sync.on('translate.owl.carousel', function(event){

                    TweenMax.to(textInside,0.4, 
                        {
                            opacity:0,
                            ease: Power4. easeOut
                        }

                    )


                      TweenMax.to(imageThumb,0.8, 
                        {
                            transformOrigin:"50% 50%",
                            scale:0,
                            ease: Power1.easeOut,
                            autoAlpha:0,
                            rotation:0,
                            scale:0
                        }
                    )
            });*/

            /*
            sync.on('translated.owl.carousel', function(event){

                    TweenMax.to(textInside,2.0, 
                        {
                            autoAlpha:1,
                            ease: Power1. easeOut,
                            delay:0.2
                        }

                    )

                      TweenMax.to(imageThumb, 1.5, 
                            {
                                transformOrigin:"50% 50%",
                                scale:1,
                                ease: Power4.easeOut,
                                autoAlpha:1,
                                delay:0.4,
                                rotation:0,
                                scale:1
                            }
                        )
            });*/

    });


     var bulletSlide = 0;
     var slidesRotator = $('.text-rotator').find('.text-item');
     var textItemCount = slidesRotator.length;

     var bulletList = $('.bullet-list');

     if(!bulletList.length) {
        bulletList = $('<ul></ul>', {
            'class': 'bullet-list'
        }).appendTo('.text-rotator');
     }



        for(var i=0; i < textItemCount; i++) {

             var paginationBull = $('<li></li>', {

            }).appendTo(bulletList);

            var circleBullet = $('<span></span>', {
                'class':'bullet'
            }).appendTo(paginationBull);
            
        }


        var owlContr =  $('.grid-carousel.owl-carousel').owlCarousel();

        $(".prevArr").click(function () {
            owlContr.trigger('prev.owl.carousel');
        });

        $(".nextArr").click(function () {
           owlContr.trigger('next.owl.carousel');
        });

}

/*var owlTest = $('.owl-carousel').owlCarousel();

        $(".prev").click(function () {
            owlTest.trigger('prev.owl.carousel');
        });

        $(".next").click(function () {
           owlTest.trigger('next.owl.carousel');
        });*/



    
personGridRotator();

    function gridSlider() {
        $('.grid-rotator.owl-carousel').owlCarousel({
                items:1,
                loop:true,
                nav:false,
                dots:true,
                autoplay:true,
                autoplayTimeout:6000,
                mouseDrag:false,
                freeDrag:false
            });
    }

     var options = {
            items:1,
            loop:true,
            nav:false,
            dots:true,
            autoplay:true,
            autoplayTimeout:4500,
            mouseDrag:false,
            freeDrag:false
        }

    /*var owl = $('.testimonial-grid-rotator.owl-carousel').owlCarousel(options);*/

    function gridTestimonialSlider(owl) {
        owl.trigger('destroy.owl.carousel');
        owl.html(owl.find('.owl-stage-outer').html()).removeClass('owl-loaded');
        owl.owlCarousel(options);
    }

    function createPreloader() {
        var preloader = $('#preloader');

        if(!preloader.length) {
            preloader = $('<div></div>', {

                "id":"preloader",
                "class":"preloader"

            }).appendTo('body.home');
        } 
    }

    function slideContactPanel() {

        var contactLeftPanel = $('.contact-form-panel');

        $('.panel-left-email').on('click', function(e){
            e.preventDefault();
            var that = $(this);

            if($(window).width() < 992) {
                $('html, body').addClass('overflow-hidden');

            }else {

                $('html, body').removeClass('overflow-hidden');
            }

            contactLeftPanel.addClass('opened-contant-panel');

        });


        $('.close-panel-btn').on('click', function(){

            if($(window).width() < 992) {
               $('html, body').removeClass('overflow-hidden');

            }

            contactLeftPanel.removeClass('opened-contant-panel');

        });

    }

    function footerScroll() {

        if($(window).scrollTop() > 40) {

                TweenMax.to('.page-footer', 0.6, 

                     {
                        bottom:0,
                        ease:Power4.easeInOut
                     }
                )

            }else if($(window).scrollTop() < 40) {

                    TweenMax.to('.page-footer', 0.6, 

                         {
                           bottom:'-100%',
                           ease:Power1.easeInOut
                         }

                    )
            }
    }



    function footerScrollGrid() {

        if($(window).scrollTop() > 40) {

                TweenMax.to('.page-footer', 0.6, 

                     {
                        bottom:0,
                        ease:Power4.easeInOut
                     }
                )

            }else if($(window).scrollTop() < 40) {

                    TweenMax.to('.page-footer', 0.6, 

                         {
                           bottom:'-100%',
                           ease:Power1.easeInOut
                         }

                    )
            }
        
    }

    function footerMainGrid() {
        
            if($(window).scrollTop() > 40) {

                TweenMax.to('.grid-footer', 0.6, 

                     {
                        bottom:0,
                        ease:Power4.easeInOut
                     }
                )

            }else if($(window).scrollTop() < 40) {

                    TweenMax.to('.grid-footer', 0.6, 

                         {
                           bottom:'-100%',
                           ease:Power1.easeInOut
                         }

                    )
            }
    }

    function footerHexagon() {

        if($(window).scrollTop() > 30) {

                TweenMax.to('.hexagon-footer', 0.6, 

                     {
                        bottom:0,
                        ease:Power4.easeInOut
                     }
                )

        }else if($(window).scrollTop() < 30) {

                TweenMax.to('.hexagon-footer', 0.6, 

                     {
                       bottom:'-100%',
                       ease:Power1.easeInOut
                     }

                )
        }
        
    }

        // Home slider intro href
        var mediaQuery = window.matchMedia('(min-width: 1025px)');
        mediaQuery.addListener(clickAnimation);

        function clickAnimation() {

            if(mediaQuery.matches) {
                var sliderBtn = $('.slider-column').find('.slide-btn');
                var slideHome = $('.countries-choose li').find('a');

                sliderBtn.on('click', function(event){
                   
                    var that = $(this);
                    var clickedLink = that.attr('href');
                    event.preventDefault();
                    

                    TweenMax.to($('.home-header'), 0.3, 
                        {
                           autoAlpha:0,
                           top:-100,
                           ease:Power2.easeOut
                        }
                    )

                    TweenMax.staggerTo($('.pagination-list li'), 0.3, 
                        {
                            delay:0.3,
                            autoAlpha:0,
                            ease: SlowMo.easeIn,

                            onComplete:function() {
                                TweenMax.to($('.pagination-nav'),0.4,

                                    {
                                        opacity:0,
                                        left:"-100%",
                                        ease:Power4. easeOut
                                    }

                                )
                            }

                        },0.2
                    )

                    TweenMax.to($('.live-chat-wrapper'),0.3,
                        {
                            bottom:-200,
                            delay:0.8,
                            ease: SlowMo.easeIn,

                            onComplete:function() {
                                 TweenMax.to($('body'),0.5,

                                    {
                                        autoAlpha:0, 
                                        ease:Power2.easeOut,
                                         onComplete:function() {
                                            window.location = clickedLink;
                                         }
                                    } 

                                )
                            }
                        }
                    )

                });


                slideHome.on('click', function(event){
                   
                    var that = $(this);
                    var clickedLink = that.attr('href');
                    event.preventDefault();
                    

                    TweenMax.to($('.home-header'), 0.3, 
                        {
                           autoAlpha:0,
                           top:-100,
                           ease:Power2.easeOut
                        }
                    )

                    TweenMax.staggerTo($('.pagination-list li'), 0.3, 
                        {
                            delay:0.3,
                            autoAlpha:0,
                            ease: SlowMo.easeIn,

                            onComplete:function() {
                                TweenMax.to($('.pagination-nav'),0.4,

                                    {
                                        opacity:0,
                                        left:"-100%",
                                        ease:Power4. easeOut
                                    }

                                )
                            }

                        },0.2
                    )

                    TweenMax.to($('.live-chat-wrapper'),0.3,
                        {
                            bottom:-200,
                            delay:0.8,
                            ease: SlowMo.easeIn,

                            onComplete:function() {
                                 TweenMax.to($('body'),0.5,

                                    {
                                        autoAlpha:0, 
                                        ease:Power2.easeOut,
                                         onComplete:function() {
                                            window.location = clickedLink;
                                         }
                                    } 

                                )
                            }
                        }
                    )

                });

            } else {

                /*Tablet view*/
                 TweenMax.to($('body'),0.5, 
                    {
                        autoAlpha:1, 
                        ease:Power2.easeOut
                    }
                )
            }
        }


        moveToPageUrl = function(event) {

            event.preventDefault();
    
            var that = $(this);
            var tileClicked = that.attr('href');

            TweenMax.to($('body'),0.7, 
                    {
                        autoAlpha:0,
                        ease:Power4.easeInOut,
                        onComplete:function() {
                            window.location = tileClicked;
                        }
                    }
                )
        };


         

    
    // Animation on click main navigation

       var viewportQuery = window.matchMedia('(min-width: 1024px)');
        viewportQuery.addListener(menuClick);

        function menuClick(viewportQuery) {
            if (viewportQuery.matches) {
                var mainNavigation = $('.main-navigation-list a');
                var tilesLinks = $('.tile').find('a');
                var postNavigation = $('.page-bottom-area').find('a');
                var breadCrumbLink = $('.breadcrumbs-list').find('a');
                var blogPostLink = $('.blog-post').find('a');
                var latestPostsLink = $('.category-list li').find('a');
                var bottomNavigationLink = $('.footer-menu-list li').find('a');


                postNavigation.on('click',moveToPageUrl);
                breadCrumbLink.on('click',moveToPageUrl);
                tilesLinks.on('click',moveToPageUrl);
                latestPostsLink.on('click',moveToPageUrl);



                bottomNavigationLink.on('click',function(event){
                    event.preventDefault();
                   

                    var that = $(this),
                        bottomLinkClicked = that.attr('href');

                       TweenMax.to($('.footer-scroll-grid'), 1.0,

                            {
                                ease:Power4.easeInOut,
                                bottom:'-100%',
                                onComplete:function() {
                                    TweenMax.to($('body'), 0.7,
                                        {
                                            autoAlpha:0, 
                                            ease:Power4.easeInOut,
                                            onComplete:function() {
                                                window.location = bottomLinkClicked;
                                            }
                                        }
                                    )
                                }
                            }
                        )
                });


                //Main navigation functionality
                mainNavigation.on('click', function(event){

                    event.preventDefault();

                    if (event.target !== this) return;
                    
                   
                    var that = $(this);

                    var clickedLink = that.attr('href');

                    TweenMax.to($('.left-navigation'), 0.5,

                        {
                             ease:Power0.easeNone,
                             className:"-=display-nav", 

                            onComplete:function() {

                                TweenMax.to($('body'), 0.7,
                                    {
                                        autoAlpha:0, 
                                        ease:Power4.easeInOut,
                                        onComplete:function() {
                                            window.location = clickedLink;
                                        }
                                    }
                                )

                            }

                        }
                    )

                    TweenMax.to($('#switcher'), 0.2,
                            {
                                ease:Power0.easeNone,
                                className:"-=open-nav"
                            }
                        )
                });

            } else {
                TweenMax.to($('body'),0.7,
                    {
                        autoAlpha:1, 
                        ease:Power4.easeInOut,
                        onComplete:function() {
                            TweenMax.to($('html'), 0.1, 
                                {
                                    className:"-=changeBck"
                                }
                            )
                        }
                    }
                )
            }
        }

        function videoPopUp() {
            if($('.blog-post-link')!==0) {
                 $(".blog-post-link").magnificPopup({
                    type:'inline',
                    mainClass: 'mfp-fade',
                    removalDelay: 100,
                    preloader: true,
                    midClick: true,

                    gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    tCounter: '%curr% of %total%',
                    preload: [0,1] 
                    },

                        image: {
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                        titleSrc: function(item) {
                        return /*'<h4 class="lb-title">' + item.el.attr('title') + '</h4>' + */'<p class="lb-description">' + item.el.attr('data-description') + '</p>';
                    }
                }



                });
                  $('.mfp-close').magnificPopup('close');
            }else {
                return;
            }

        }

       


        function matchHeight() {
            if($('.download-section-wrapper').length!==0) {

                $('.downloads-inner').each(function(){
                    var that = $(this);

                    that.children('.download-item').matchHeight({byRow:false});
                });

            }

             if($('#category-filter-section').length!==0) {
                $('.download-post').matchHeight({property: 'height'});
             }

              $('.slider-column').matchHeight({property: 'height'});  

            
        }

    $(function () {

        if($(window).width() <= 992) {
            sections.setMinHeight();
            sections.css('height','auto')
        }else {
            sections.setSectionHeight();
            sections.matchHeight({ property: 'min-height' });
        }

        if($(window).width() <= 1170) {
            $('.backface').off();
            
        } else {
            desktopHover();
        }

        clickAnimation(mediaQuery);
        menuClick(viewportQuery);
        
        containerWidth();
        clickSwitcher();
        homeSlider();
        gridSlider();
        createPreloader();
        dropDownNavigation();
        //tilesWrapper.setMinHeight();
        hoverTiles();
        playHexagonAnimation();
        onResize();
        slideContactPanel();
        scrollToTeam();
        videoPopUp();
        createPagination();
        testimonialWrapperWidth();
        slideTestimonialWidth();
        paginationSlidesLength();
        paginationClick();
        addSpantoBtn();
        matchHeight();
        homeSlideMenu();
        slideDescriptionClass();
        downloadPopUp();
        swapHomeBtn();
        slideToDownload();
        agencyTabs();

       /* if($('.testimonial-grid-rotator').length > 0) {
            gridTestimonialSlider(owl);
        }*/ 
       
    }); //End document ready


    function downloadPopUp() {
        if($('.downloads-inner')!==0) {
            var emailRequest = $('.downloads-inner').find('.email-request');

            $(emailRequest).magnificPopup({
                items: {
                    src: '#download-form-wrapper',
                    type: 'inline',
                    mainClass: 'mfp-fade',
                    removalDelay: 100,
                    preloader: true,
                    midClick: true
                }
            });
        }
    }


    function testimonialsRotator() {

        var testimonialsCarousel = $('.testimonials-slider.owl-carousel');


        testimonialsCarousel.on('initialize.owl.carousel', function(event){
            TweenMax.to($('.middle-circle'), 1.1, 
                    {
                        delay:0.2,
                        autoAlpha:1,
                        left:0 ,
                        ease: Power4.easeOut
                    }
                )


            TweenMax.to($('.small-circle'), 1.1, 
                    {
                        delay:0.6,
                        autoAlpha:1,
                        right:"7%",
                        ease: Power2.easeOut
                    }
                )

            TweenMax.to($('.bottom-circle'), 1.5, 
                    {
                       rotation:360,
                        transformOrigin:"50% 50%",
                        scale:1,
                        ease: Power2.easeOut,
                        opacity:1,
                        delay:0.8,
                    }
                )
        });


        testimonialsCarousel.owlCarousel({
            items:1,
            dots:true,
            mouseDrag:false,
            freeDrag:false,
            autoplay:true,
            autoplayTimeout:20000,
            loop:true
        });


        testimonialsCarousel.on('translate.owl.carousel', function(event){

            TweenMax.to($('.middle-circle'), 0.2, 
                        {
                            opacity:0,
                            left:"20%" ,
                             ease: Power4.easeOut
                        }

                    )

            TweenMax.to($('.small-circle'),  0.2, 
                    {
                       
                        opacity:0,
                        right:"30%",
                        ease: Power4.easeOut
                    }
                )


            TweenMax.to($('.bottom-circle'), 0.2, 
                    {
                       rotation:0,
                        transformOrigin:"50% 50%",
                        scale:0,
                        ease: Power4.easeOut,
                        opacity:0
                    }
                )

        });


        testimonialsCarousel.on('translated.owl.carousel', function(event){
            TweenMax.to($('.middle-circle'), 1.1, 
                    {
                        autoAlpha:1,
                        left:0 ,
                        ease: Power4.easeOut
                    }
                )

            TweenMax.to($('.small-circle'), 1.1, 
                    {
                        autoAlpha:1,
                        delay:0.4,
                        right:"10%",
                        ease: Power4.easeOut
                    }
                )


            TweenMax.to($('.bottom-circle'), 1.1, 
                    {
                       rotation:360,
                        transformOrigin:"50% 50%",
                        scale:1,
                        ease: Power4.easeOut,
                        opacity:1,
                        delay:0.6
                    }
                )
        });


        var owlTest = $('.owl-carousel').owlCarousel();

        $(".prev").click(function () {
            owlTest.trigger('prev.owl.carousel');
        });

        $(".next").click(function () {
           owlTest.trigger('next.owl.carousel');
        });


        var testimonialsArrows = $('.footer-testimonial-rotator').find('span');

        testimonialsArrows.on('mouseenter', function(){
             TweenMax.to(this, 1.2, {scale:1.4, ease: Power1.easeOut})
        }).on('mouseleave', function(){
            TweenMax.to(this, 0.5, {scale:1, ease: Power4.easeOut})
        });

    }


    //Single post header animation

    var t = TweenMax.to(singleHeader, 2.5,{
            autoAlpha:0,
            paused:true, 
             ease: Power2.easeOut
        }),

    scrollThreshold = {"start":100, "stop":400};


    $(window).on('scroll', function () {

        var currentScroll = $(window).scrollTop(),
            currentProgress = ( currentScroll - scrollThreshold.start ) / ( scrollThreshold.stop - scrollThreshold.start );
            if(currentScroll > scrollThreshold.start && currentScroll < scrollThreshold.stop){
                t.progress(currentProgress);
              }else if( currentScroll < scrollThreshold.start ){
                t.progress(0);
              }else if( currentScroll < scrollThreshold.stop ){
                t.progress(1);
              }

         addClassonScroll();

    
           var win = $(window);

        if(win.scrollTop() > 50) {
            TweenMax.to('.footer-video', 1.0, 

             {
                bottom:0
             }

            )
        }else if(win.scrollTop() < 50) {
            TweenMax.to('.footer-video', 1.0, 

             {
               bottom:'-100%'
             }

            )
        }


        footerMainGrid();
        footerHexagon();
        footerScroll();
         footerScrollGrid();


        function changeColorScroll() {
            var icoLink = $('.ico-link').find('a');
            if($('.slides-container').hasClass('animate-color')){
                icoLink.addClass('change-color-link');
            }else {
                icoLink.removeClass('change-color-link');
            }
        };

        changeColorScroll();

    });

    // Browser's back handler for home page slides

    function onPopStateHandler(event) {
        var state = event.state;
        if (state) {
            wasUsedBackButton = true;
            findHash();

            TweenMax.to($('body'),0.4,
                {
                   autoAlpha:1,
                    ease: SlowMo.easeIn
                }
            )
        }
    }

    // Reset back browser button and go back 
    if($('body').hasClass('team')) {
            window.addEventListener('popstate', function(event) {
            window.history.back(-1);
        });
    }
   
    

    $(window).unload(function(){
        TweenMax.to($('body'),0.4,
            {
                autoAlpha:1,
                ease: SlowMo.easeIn
            }
        )
    });

    $(window).on('load', function () {

        startAutoPlayTestimonials();
        
        setTimeout(function(){
            $('.back-btn').addClass('show-link');
        },800);

        function nonIntroChat() {
            var chatIcon = $('.non-intro-chat');


            if(chatIcon.length > 0) {
                TweenMax.to($('.non-intro-chat'),0.5,
                {
                    bottom:0,
                    ease: SlowMo.easeIn
                });
            }
        }

        nonIntroChat();

        /*setTimeout(function() {
            owl.owlCarousel('invalidate', 'all').owlCarousel('refresh');
        },6400);*/

        var hash = window.location.hash.replace("#","");

         if(hash==='') {
            // Wywolanie bez hasha
            linksPag.filter(":first").trigger("click");


        }else {
            findHash();

            // Removes hash from browsing history
            $('.close-panel-team').trigger('click');
        }


        setTimeout(function () {
            linksPag.on('click', scrollToSection);
            findHash();
        }, 200);

        setTimeout(function () {
            window.onpopstate = onPopStateHandler;
        }, 1000);

            startAutoPlay(true);
            flipTiles();
            flipPageTitle();


           var options = {
            dragLockToAxis: true,
            dragBlockHorizontal: true
        };

        // Enable hammer text selection
        delete Hammer.defaults.cssProps.userSelect;
        var hammertime = new Hammer(document.querySelector('body'));

        hammertime.on('dragright swiperight', function(e) {
            $('#switcher').addClass('open-nav');
            leftNavigation.addClass('display-nav');
          //  tilesWrapper.addClass('change-index');
            $('.home-header').addClass('header-index');
        });


        hammertime.on('dragleft swipeleft', function(e) {
            $('#switcher').removeClass('open-nav');
            leftNavigation.removeClass('display-nav');
           // tilesWrapper.removeClass('change-index');
            $('.home-header').removeClass('header-index');
        });

        testimonialsRotator();
        

        // Slides intro animation
        TweenMax.to($('body'), 1.2,
            {
                autoAlpha:1, 
                ease:Power4.easeInOut,
                onComplete:function() {
                    $('#preloader').remove();
                    $('html').removeClass('changeBck');

                /*tl.to($('.section'), 0.5, 

                    {
                        autoAlpha:1,
                        ease: Power2. easeOut,

                        onComplete:function() {
                            TweenMax.to($('.slide-header'), 1.2, 
                                {
                                    autoAlpha:1,
                                    ease: SlowMo.easeIn,
                                    x:0

                                }
                            )

                            TweenMax.to($('.slider-column-left p'), 1.2, 
                                {
                                    autoAlpha:1,
                                    ease: SlowMo.easeIn,
                                    x:0,
                                    delay:0.2
                                }
                            )

                            TweenMax.staggerTo($('.countries-choose li'), 0.3, 
                                {
                                    autoAlpha:1,
                                    delay:0.5,
                                    ease: SlowMo.easeIn
                                }, 0.2
                            )

                            TweenMax.to($('.slider-column-left .slide-btn'), 1.2, 
                                {
                                    autoAlpha:1,
                                    ease: SlowMo.easeIn,
                                    y:0,
                                    delay:0.8
                                }
                            )
                        }
                    }
                )*/
                   
                }
            }
        )


                    tl.to($('.pagination-nav'), 0.4,

                        {
                            opacity:1,
                            left:"0",
                            delay:0.5,
                            ease:Power4. easeOut,

                            onComplete:function() {
                                TweenMax.staggerTo($('.pagination-list li'), 0.3, 
                                    {
                                        autoAlpha:1,
                                        ease: SlowMo.easeIn
                                    }, 0.2
                                )

                                TweenMax.to($('.live-chat-wrapper'),0.5,
                                    {
                                        bottom:0,
                                        delay:0.4,
                                        ease: SlowMo.easeIn
                                    }
                                )
                            }
                        }
                    )

                    TweenMax.to($('.home-header'), 0.7,

                        {
                            autoAlpha:1,
                            delay:0.2,
                            ease: Power4.easeOut
                        }
                    )

    }).on('resize', function () {

        if($(window).width() <= 992) {
            sections.setMinHeight();
            sections.css('height','auto')
        }else {
            sections.setSectionHeight();
            sections.matchHeight({ property: 'min-height' });
        }

        //tilesWrapper.setMinHeight();
        addClassonScroll();

        setTimeout(function(){

            hoverTiles();
            homeSlider();
            gridSlider();

        },0);

        // Fix Owl carousel on resize
        /*setTimeout(function() {
            owl.owlCarousel('invalidate', 'all').owlCarousel('refresh');
        },400);*/

        if($(window).width() <= 1025) {
            $('.container-hover').off();
        }

        if($(window).width() <= 1170) {

            $('.backface').off();
            
        } else {
           desktopHover();
        }

        if($(window).width() > 992) {
            $('html, body').removeClass('overflow-hidden');
        }

        refresh = true;
        gotoSlide(currentSlideID);
        gotoTestimonial(currentTestimonialSlide);
        containerWidth();
        onResize();
        
        findHash();
        slideContactPanel();
        testimonialWrapperWidth();
        slideTestimonialWidth();

        clearTimeout(resizeTimer);

        resizeTimer = setTimeout(function() {
            
          //Some code
          

        },250);


    });


})(jQuery);
