function footerMainGrid() {
        var lastEl = $('.inside-container').find('.tile:last-child');

        if ($(window).innerWidth() <= 1366) {

            if($('.inside-container').length > 0) {

            

             var tween = TweenMax.to($('.grid-footer'), 0.4,
                {
                    y:0,
                    ease:Cubic.easeOut
                }
            );


                var controller = new ScrollMagic.Controller();
                var scene = new ScrollMagic.Scene({
                    triggerElement: lastEl, 
                    reverse: true,
                    offset: 150
                }).setTween(tween)
                    .addTo(controller);
            }

        }else {

            var tween = TweenMax.to($('.grid-footer'), 0.4,
                {
                    y:0,
                    ease:Cubic.easeOut
                }
            );


                var controller = new ScrollMagic.Controller();
                var scene = new ScrollMagic.Scene({
                    triggerElement: lastEl, 
                    reverse: true,
                    offset: 60
                }).setTween(tween)
                    .addTo(controller);
            
        }
    }


    function footerHexagon() {
        if ($(window).innerWidth() > 1900) {

            if($('.inside-container').length > 0) {
                var lastHex = $('.inside-container').find('.hex-item:last-child');

                // console.log('1900');

                var tween = TweenMax.to($('.hexagon-footer'), 0.4,
                    {
                        y:0,
                        ease:Cubic.easeOut
                    }
                );


                var controller = new ScrollMagic.Controller();
                var scene = new ScrollMagic.Scene({
                    triggerElement: lastHex, 
                    reverse: true,
                    offset: -400
                }).setTween(tween)
                    .addTo(controller);
         
            }

        }else if($(window).innerWidth() > 980) {
            if($('.inside-container').length > 0) {
                var lastHex = $('.inside-container').find('.hex-item:last-child');

                var tween = TweenMax.to($('.hexagon-footer'), 0.4,
                    {
                        y:0,
                        ease:Cubic.easeOut
                    }
                );


                //console.log('less then 1900');


                var controller = new ScrollMagic.Controller();
                var scene = new ScrollMagic.Scene({
                    triggerElement: lastHex, 
                    reverse: true,
                    offset: 100
                }).setTween(tween)
                    .addTo(controller);
         
            }
        } else {
            return;
        }
    }







    function footerScroll() {
        if ($(window).innerWidth() > 992) {

           // var footerAnimation = new TweenMax({paused:true});

            var tween = TweenMax.to($('.page-footer'), 0.4,
                {
                    y:0,
                    ease:Cubic.easeOut
                }
            );

    
                var controller = new ScrollMagic.Controller();
                var scene = new ScrollMagic.Scene({
                    triggerElement: '.page-inner', 
                    triggerHook: "onEnter", 
                    reverse: true,
                    offset: $('.page-inner').innerHeight() - 150
                }).setTween(tween)
                    .addTo(controller);
        }   
    }