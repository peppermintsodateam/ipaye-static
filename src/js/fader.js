function createOverlay() {
        var overlay = $('#overlay');

        if(!overlay.length) {
            overlay = $('<div></div>', {
                "class":"overlay",
                "id":"overlay",
                on: {
                    click: function() {
                       closeOverlay(); 
                       $('#switcher').removeClass('open-nav');
                       leftNavigation.removeClass('display-nav');
                        tilesWrapper.removeClass('change-index');
                    }
                }

            }).appendTo('body.home');
        }

        return overlay;
    }

    function showOverlay() {
        var overlay = createOverlay();

        overlay.css({
            width: doc.width(),
            height: doc.height()
        });

        overlay.fadeIn(500);
    }

function closeOverlay() {
        var overlay = $("#overlay");
        overlay.fadeOut(500);
    }