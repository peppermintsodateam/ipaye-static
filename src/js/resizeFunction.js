 // Tiles grid function
    function onResize() {

        var front = $('.front');
        var back = $('.back');

        front.css('height', $('.tile').height() - 3);
        back.css('height', $('.tile').height() - 3);

        front.css('width', $('.tile').width() - 3);
        back.css('width', $('.tile').width() - 3);

        if($(window).width() >= 1366) {

            var wrapperWidth = Math.floor($('.all-tiles-wrapper').width() / 4);
            var rowheight = Math.floor($(window).height() / 4);

            


        }else if($(window).width() > 1024) {
            var wrapperWidth = Math.floor($('.all-tiles-wrapper').width() / 3);
            var rowheight = Math.floor($(window).height() / 3);

        } else if($(window).width() > 640) {

            var wrapperWidth = Math.floor($('.all-tiles-wrapper').width() / 2);
            var rowheight = Math.floor($(window).height() / 2);

        } else {
            var wrapperWidth = Math.floor($('.all-tiles-wrapper').width());
            var rowheight = Math.floor($(window).height());
        }



        var tileHeight = $('.tile').width();

        var front = $('.front');
        var back = $('.back');
        var leftPanel = $('.left-panel-wrapper');

        $('.tile').each(function(index) {
            $(this).outerHeight(wrapperWidth);
            $(this).outerWidth(wrapperWidth);

        });

        $('.all-tiles-wrapper').css('width', $(window).width() - $('.left-panel-wrapper').outerWidth());

        front.height(wrapperWidth);
        back.height(wrapperWidth);

        front.width(wrapperWidth);
        back.width(wrapperWidth);

        

            $('.top-left').find('.front').height(wrapperWidth - 2 + "px");
            $('.top-left').find('.back').height(wrapperWidth - 2 + "px");

            $('.top-left').find('.front').width(wrapperWidth - 2 + "px");
            $('.top-left').find('.back').width(wrapperWidth - 2 + "px");

            
            $('.top-tile-right').find('.front').height(wrapperWidth - 2 + "px");
            $('.top-tile-right').find('.back').height(wrapperWidth - 2 + "px");

            $('.top-tile-right').find('.front').width(wrapperWidth - 2 + "px");
            $('.top-tile-right').find('.back').width(wrapperWidth - 2 + "px");


            $('.top-tile-right-curve').find('.front').height(wrapperWidth - 2 + "px");
            $('.top-tile-right-curve').find('.back').height(wrapperWidth - 2 + "px");

            $('.top-tile-right-curve').find('.front').width(wrapperWidth - 2 + "px");
            $('.top-tile-right-curve').find('.back').width(wrapperWidth - 2 + "px");


            $('.curve-top').find('.front').height(wrapperWidth - 2);
            $('.curve-top').find('.back').height(wrapperWidth - 2);

            $('.curve-top').find('.front').width(wrapperWidth - 2);
            //$('.curve-top').find('.back').width(wrapperWidth - 2);

            $('.tile-purple-border').find('.front').width(wrapperWidth - 2);
            //$('.tile-purple-border').find('.back').width(wrapperWidth - 2);

            $('.tile-purple-height').find('.front').height(wrapperWidth - 2);
            $('.tile-purple-height').find('.back').height(wrapperWidth - 2);

            $('.tile-purple-height').find('.front').width(wrapperWidth - 2);
            $('.tile-purple-height').find('.back').width(wrapperWidth - 2);

            $('.tile-purple-width').find('.front').width(wrapperWidth - 2);
            //$('.tile-purple-width').find('.back').width(wrapperWidth - 2);

            $('.tile-purple-width').find('.front').height(wrapperWidth - 2);
            $('.tile-purple-width').find('.back').height(wrapperWidth - 2);

    }