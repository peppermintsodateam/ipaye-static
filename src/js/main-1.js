(function ($) {

// VARIABLES
    var mainContent = $('.main-slides-wrapper');
    var slidesContainer = mainContent.find('.slides-container');
    var slides = slidesContainer.children();
    var slidesWidth = slides.width($(window).width());
    var slidesCount = slides.length;
    var currentSlideID = 0;
    var homeHeader = $('.home-header');

    var sections = $('.section');
    var curSlide = slides.first();
   
    var linksPag = $('.pagination-list li').find('span');
    var loadedPage = true; // After page is loaded is zero
    var isAnimating = false;
    var refresh = false;
    var wasUsedBackButton = false;

    var slideIntervalId = 0;

    

    /*Height section plugin*/

    $.fn.setSectionHeight = function () {
        return this.css('height', $(window).height());
    }

    function containerWidth() {
        slidesContainer.css({
            'width': (slidesCount * 100 + "%")
        });

        slides.width($(window).width());
    }


     function gotoNextSlide() {

        var slideToGo = currentSlideID + 1;
        if (slideToGo >= slidesCount) {
            slideToGo = 0;
        }
        gotoSlide(slideToGo,false);
    }

    function startAutoPlay(immiediate) {
        //Przerywam interwal i rozpoczynam nowy
        clearInterval(slideIntervalId);
        slideIntervalId = setInterval(gotoNextSlide, 6000);
        if(immiediate) {
            gotoNextSlide(); 
        }
    }

    function stoponHover() {
            sections.on('mouseover',function(){
                clearInterval(slideIntervalId);
                
            }).on('mouseout',function(){
                startAutoPlay(false);
            });

            linksPag.on('mouseover',function(){
                clearInterval(slideIntervalId);

            }).on('mouseout',function(){
                startAutoPlay(false);
            });
        }

        stoponHover();


    function gotoSlide(slideID, loadedPage) {
        loadedPage = loadedPage;
        currentSlideID = slideID;
        linksPag.eq(slideID).click();
    }


    scrollToSection = function (e) {

        e.preventDefault();
        var that = $(this);
        var targetId = that.data('slider');
       // console.log('targetID', targetId);
        if (!targetId) return;

        $('.pagination-list').find('span').filter(':first').addClass('active-link');
        $('.pagination-list').find('span').removeClass('active-link');
        that.addClass('active-link');


        var target = $('.slides-container').find('section').filter(function () {
            // Pod target jest sekcja, ktora "maczuje" klikniety przycisk paginacji z sekcja
            return $(this).data('slider') === targetId;
        });

        //console.log('target', target);

        var matchTarget = targetId.split('-');
        // Myslnik jest znakiem rozdzielajacym elementy
        //Split dzieli na tablice 2 elementowa, poniewaz to chcemy uzyskac case-1

        currentSlideID = Number(matchTarget[matchTarget.length - 1]) - 1; // W zaleznosci ktory element paginacji jest klikniety
        var offset = currentSlideID * $(document.body).width();


        // jesli nie on resize to animuj inaczej ustaw odrazu w
        // odpowiedniej pozycji
        if (!refresh) {
            var time = (loadedPage) ? 2 : 0.5; // Po zaladowaniu strony animacja przesuniecia do slajdu
            loadedPage = false;
            TweenMax.to($('.slides-container'), time,
                {
                    marginLeft: -offset,
                    onComplete: function () {
                        isAnimating = false;
                        var currentContainer = slides.eq(currentSlideID);
                    }
                }
            )

        } else {
            $('.slides-container').css('margin-left', -offset);
        }

        refresh = false;

        // backButton - dodaj urla do historii przegladarki tylko jesli nie korzystasz z przyciskow przegladarki back lub next
        if (typeof history.pushState !== "undefined" && !wasUsedBackButton) {
            // Zmiana url w pasku przegladarki
            var id = target.data('slider');
            history.pushState({id: id}, '', "#" + id);
            //console.log('id', id);
        }

        wasUsedBackButton = false;

    };


    function findHash() {

        // Replace usuwa has z data 
        var hash = (window.location.hash).replace('#', "");
        $(linksPag).filter(function () {
            return $(this).data('slider') === hash;
            // Trigger click robi klika na paginacji i przesuwa po załadowaniu strony ze sliderem do odpowiedniego slajdu
        }).trigger("click");

        // jesli nie ma wybranego hasha to zrob click na pierwszym elemencie
        if (hash === '') {
            $(linksPag)[0].click();
        }
    }


    jQuery.browser = {};
    (function () {
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
    })();


    function clickSwitcher() {
        $('#switcher').on('click', function () {
            var that = $(this);
            that.toggleClass('open-nav');

        });
    }

    function homeSlider() {
        $('.home-rotator.owl-carousel').owlCarousel({
                items:1,
                loop:true,
                nav:false,
                dots:true,
                autoplay:true,
                autoplayTimeout:6000,
                mouseDrag:false,
                freeDrag:false
            });
    }

    function createPreloader() {

        var preloader = $('#preloader');

        if(!preloader.length) {
            preloader = $('<div></div>', {

                "id":"preloader",
                "class":"preloader"

            }).appendTo('body.home');
        } 

    }


    $(function () {
        sections.setSectionHeight();
        containerWidth();
        clickSwitcher();
        homeSlider();
        createPreloader();

    }); //End document ready

    $(window).on('scroll', function () {

    });

    // Browser's back handler

    function onPopStateHandler(event) {
        var state = event.state;
        if (state) {
            wasUsedBackButton = true;
            findHash();
        }
    }

    $(window).on('load', function () {

        setTimeout(function () {
            linksPag.on('click', scrollToSection);

            findHash();
        }, 0);

        setTimeout(function () {
            window.onpopstate = onPopStateHandler;
        }, 1000);

        startAutoPlay(true);


           var options = {
            dragLockToAxis: true,
            dragBlockHorizontal: true
        };

        // Enable hammer text selection
        delete Hammer.defaults.cssProps.userSelect;
        var hammertime = new Hammer(document.querySelector('body'));

        hammertime.on('dragleft dragright swipeleft swiperight', function(e) {
            console.log('swipe-left');
            var offset = currentSlideID * $(document.body).width();

        });

        // Slides intro animation
        TweenMax.to($('.home-header'), 1.0,

            {
                autoAlpha:1,
                top:0,
                ease: SlowMo.easeIn,
                onComplete:function() {
                    $('#preloader').remove();
                    TweenMax.to($('.main-slides-wrapper'), 2.0,

                        {
                            autoAlpha:1,
                            ease: SlowMo.easeIn
                        }

                    )
                }
            }
        )

        TweenMax.staggerFrom($('.pagination-list li'), 1.5,
                {
                    delay:1.5,
                    autoAlpha:0,
                     ease: SlowMo.easeIn,
                     onComplete:function() {
                        TweenMax.to($('.live-chat-wrapper'),0.6,
                                {
                                    bottom:0,
                                    ease: SlowMo.easeIn
                                }

                            )
                     }

                } ,0.2
            )

    }).on('resize', function () {
        sections.setSectionHeight();

        refresh = true;
        gotoSlide(currentSlideID);
        containerWidth();
    });


})(jQuery);
