window.onload = function(){
	var touchSlider = document.querySelector('#touch-slider');
	var slidesContainer = document.querySelector('.slides-container');
	var slides = slidesContainer.querySelectorAll('.section');
	var slidesCount = slides.length;
	var slideWidth = slides.window.innerWidth;
	var currentSlide = 0;

	var animate = function(moveBy) {
        $(slidesContainer).css('transform', 'translate('+moveBy+'%, 0)');
    }


    var showSlide = function(index){
		index = Math.max(0, Math.min(index, slidesCount-1));
		currentSlide = index;

		var offset = -((100/slidesCount)*currentSlide);
		animate(offset);
	}


	var hammer = Hammer(slidesContainer, {
		prevent_default: true
	});

	hammer.on('dragleft dragright swipeleft swiperight', function(e){
            var slideOffset = -(100/slidesCount)*currentSlide;
            var dragOffset = ((100/slideWidth)*e.gesture.deltaX) / slidesCount;

             // We define variable which fires dragging (movement)
             var m = slideOffset + dragOffset;

             animate(m);
        });


	hammer.on('release', function(e){
		if(Math.abs(e.gesture.deltaX) > 300){
			if(e.gesture.direction == 'right'){
				showSlide(currentSlide-1);
			}else{
				showSlide(currentSlide+1);
			}
		}else{
			showSlide(currentSlide);
		}
	});

	hammer.on('doubletap', function(){
		showSlide(0);
	});
}


