(function($) {

var WIDTH;
var HEIGHT;
var canvas;
var con;
var g;
var pxs = new Array();
var pxa = new Array();
var pxo = new Array();
var numberObject = 6000;
var numberBall = 12;
var pulsion = 0;
var speedX = 5;
var speedY = 2;
var colorCircle = 'rgba(92,37,129,';
var colorDiamond = 'rgba(174,202,9,';
var colorHexagon = 'rgba(235,93,11,';
var opacityRect = 1;
var rint = 30;
var radius = 20;

var width = 50;  // Triangle Width
var height = 50; // Triangle Height
var padding = 20;

var canvasOne;
var conOne;
var canvasTwo;
var conTwo;


function BrowserDetection() {

    if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
        
        var ffversion = new Number(RegExp.$1);
         rint = 30;
         pulsion = 0;
    }

    else if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {

        var ieversion = new Number(RegExp.$1);
        rint = 10;
        pulsion = 0;
    }

    else if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
        var chromeversion = new Number(RegExp.$1);
         rint = 30;
         pulsion = 0;

    }
    else if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {

        var oprversion = new Number(RegExp.$1)
        rint = 30;
        pulsion = 0;
    }
    else if (/Safari[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
        var safariversion = new Number(RegExp.$1);
        rint = 30;
        pulsion = 0;

    }
    WIDTH = window.innerWidth;
    
    if (WIDTH < 1024) {
        numberObject = 19000;
        numberBall = 10;
        pulsion = 2;
        rint = 10;
    }
}

$(function(){

     BrowserDetection();

    WIDTH = window.innerWidth;
    HEIGHT = window.innerHeight;

    canvas = document.getElementById('ipaye-bck');
    $(canvas).attr('width', WIDTH).attr('height', HEIGHT);
    con = canvas.getContext('2d');
    for (var i = 0; i < numberBall; i++) {
        pxs[i] = new Circle();
        pxs[i].reset();
    }
    setInterval(draw, rint);

    canvasOne = document.getElementById('ipaye-bck-green');
    $(canvasOne).attr('width', WIDTH).attr('height', HEIGHT);

    conOne = canvasOne.getContext('2d');

    for (var i = 0; i < numberBall; i++) {
        pxa[i] = new CircleOne();
        pxa[i].reset();
    }
    setInterval(drawOne, rint);


     canvasTwo = document.getElementById('ipaye-bck-orange');
    $(canvasTwo).attr('width', WIDTH).attr('height', HEIGHT);

    conTwo = canvasTwo.getContext('2d');

      for (var i = 0; i < numberBall; i++) {
        pxo[i] = new CircleTwo();
        pxo[i].reset();
    }
    setInterval(drawTwo, rint);

});


function drawTwo() {
    conTwo.clearRect(0, 0, WIDTH, HEIGHT);
    for (var i = 0; i < pxo.length; i++) {
        pxo[i].fade();
        pxo[i].move();
        pxo[i].draw();
    }
}

function drawOne() {
    conOne.clearRect(0, 0, WIDTH, HEIGHT);
    for (var i = 0; i < pxa.length; i++) {
        pxa[i].fade();
        pxa[i].move();
        pxa[i].draw();
    }
}
   
function draw() {
    con.clearRect(0, 0, WIDTH, HEIGHT);
    for (var i = 0; i < pxs.length; i++) {
        pxs[i].fade();
        pxs[i].move();
        pxs[i].draw();
    }
}

function Circle() {
    WIDTH = window.innerWidth;
    HEIGHT = window.innerHeight;
    this.s = { ttl: numberObject, xmax: speedX, ymax: speedY, rmax: radius, rt: pulsion, xdef: 960, ydef: 540, xdrift: 4, ydrift: 4, random: true, blink: true };

    this.reset = function () {
        this.x = (this.s.random ? WIDTH * Math.random() : this.s.xdef);
        this.y = (this.s.random ? HEIGHT * Math.random() : this.s.ydef);
        this.r = ((this.s.rmax - 1) * Math.random()) + 1;
        this.dx = (Math.random() * this.s.xmax) * (Math.random() < .5 ? -1 : 1);
        this.dy = (Math.random() * this.s.ymax) * (Math.random() < .5 ? -1 : 1);
        this.hl = (this.s.ttl / rint) * (this.r / this.s.rmax);
        this.rt = Math.random() * this.hl;
        this.s.rt = Math.random() + 1;
        this.stop = Math.random() * .2 + .4;
        this.s.xdrift *= Math.random() * (Math.random() < .5 ? -1 : 1);
        this.s.ydrift *= Math.random() * (Math.random() < .5 ? -1 : 1);
    }

    this.fade = function () {
        this.rt += this.s.rt;
    }

    this.draw = function () {
        if (this.s.blink && (this.rt <= 0 || this.rt >= this.hl)) this.s.rt = this.s.rt * -1;
        else if (this.rt >= this.hl) this.reset();
        var newo = 1 - (this.rt / this.hl);
        con.beginPath();
        con.arc(this.x, this.y, this.r, 0, Math.PI * 2, true);
        con.closePath();
        var cr = this.r * newo;
        //g = con.createRadialGradient(this.x, this.y, 0, this.x, this.y, (cr <= 0 ? 1 : cr));
        //g.addColorStop(1.0, colorCircle + (newo * opacityRect) + ')');

        con.fillStyle = '#5c2581';
        con.fill();
    }

    this.move = function () {
        WIDTH = window.innerWidth;
        HEIGHT = window.innerHeight;
        this.x += (this.rt / this.hl) * this.dx;
        this.y += (this.rt / this.hl) * this.dy;
        if (this.x > WIDTH || this.x < 0) this.dx *= -1;
        if (this.y > HEIGHT || this.y < 0) this.dy *= -1;
    }

    this.getX = function () { return this.x; }
    this.getY = function () { return this.y; }
}

function CircleOne() {
    WIDTH = window.innerWidth;
    HEIGHT = window.innerHeight;
    this.s = { ttl: numberObject, xmax: speedX, ymax: speedY, rmax: radius, rt: pulsion, xdef: 960, ydef: 540, xdrift: 4, ydrift: 4, random: true, blink: true };

    this.reset = function () {
        this.x = (this.s.random ? WIDTH * Math.random() : this.s.xdef);
        this.y = (this.s.random ? HEIGHT * Math.random() : this.s.ydef);
        this.r = ((this.s.rmax - 1) * Math.random()) + 1;
        this.dx = (Math.random() * this.s.xmax) * (Math.random() < .5 ? -1 : 1);
        this.dy = (Math.random() * this.s.ymax) * (Math.random() < .5 ? -1 : 1);
        this.hl = (this.s.ttl / rint) * (this.r / this.s.rmax);
        this.rt = Math.random() * this.hl;
        this.s.rt = Math.random() + 1;
        this.stop = Math.random() * .2 + .4;
        this.s.xdrift *= Math.random() * (Math.random() < .5 ? -1 : 1);
        this.s.ydrift *= Math.random() * (Math.random() < .5 ? -1 : 1);
    }

    this.fade = function () {
        this.rt += this.s.rt;
    }

    this.draw = function () {
        if (this.s.blink && (this.rt <= 0 || this.rt >= this.hl)) this.s.rt = this.s.rt * -1;
        else if (this.rt >= this.hl) this.reset();
        var newo = 1 - (this.rt / this.hl);
        conOne.beginPath();

        conOne.moveTo(this.x + width*0.5, this.y);        // Top Corner
        conOne.lineTo(this.x, this.y + height*0.5); // Bottom Right
        conOne.lineTo(this.x + width*0.5, this.y + height);
        conOne.lineTo(this.x + width, this.y + height*0.5);
        conOne.lineTo(this.x + width*0.5, this.y);

        conOne.closePath();

        var cr = this.r * newo;
       // g = conOne.createRadialGradient(this.x, this.y, 0, this.x, this.y, (cr <= 0 ? 1 : cr));
       // g.addColorStop(this.stop, colorDiamond + (newo * opacityRect) + ')');

        conOne.fillStyle = "#aeca09";
        conOne.fill();
    }

    this.move = function () {
        WIDTH = window.innerWidth;
        HEIGHT = window.innerHeight;
        this.x += (this.rt / this.hl) * this.dx;
        this.y += (this.rt / this.hl) * this.dy;
        if (this.x > WIDTH || this.x < 0) this.dx *= -1;
        if (this.y > HEIGHT || this.y < 0) this.dy *= -1;
    }

    this.getX = function () { return this.x; }
    this.getY = function () { return this.y; }
}

function CircleTwo() {
    WIDTH = window.innerWidth;
    HEIGHT = window.innerHeight;
    this.s = { ttl: numberObject, xmax: speedX, ymax: speedY, rmax: radius, rt: pulsion, xdef: 960, ydef: 540, xdrift: 4, ydrift: 4, random: true, blink: true };

    this.reset = function () {
        this.x = (this.s.random ? WIDTH * Math.random() : this.s.xdef);
        this.y = (this.s.random ? HEIGHT * Math.random() : this.s.ydef);
        this.r = ((this.s.rmax - 1) * Math.random()) + 1;
        this.dx = (Math.random() * this.s.xmax) * (Math.random() < .5 ? -1 : 1);
        this.dy = (Math.random() * this.s.ymax) * (Math.random() < .5 ? -1 : 1);
        this.hl = (this.s.ttl / rint) * (this.r / this.s.rmax);
        this.rt = Math.random() * this.hl;
        this.s.rt = Math.random() + 1;
        this.stop = Math.random() * .2 + .4;
        this.s.xdrift *= Math.random() * (Math.random() < .5 ? -1 : 1);
        this.s.ydrift *= Math.random() * (Math.random() < .5 ? -1 : 1);
    }

    this.fade = function () {
        this.rt += this.s.rt;
    }

    this.draw = function () {
        if (this.s.blink && (this.rt <= 0 || this.rt >= this.hl)) this.s.rt = this.s.rt * -1;
        else if (this.rt >= this.hl) this.reset();
        var newo = 1 - (this.rt / this.hl);

        var facShort = 0.225,
          facLong = 1 - facShort;

        conTwo.beginPath();
        conTwo.moveTo(this.x + width*0.5, this.y);
        conTwo.lineTo(this.x, this.y + height*facShort);
        conTwo.lineTo(this.x, this.y + height*facLong);
        conTwo.lineTo(this.x + width*0.5, this.y + height);
        conTwo.lineTo(this.x + width, this.y + height*facLong);
        conTwo.lineTo(this.x + width, this.y + height*facShort);
        conTwo.lineTo(this.x + width*0.5, this.y);
        conTwo.closePath(); 

        var cr = this.r * newo;
       // g = conTwo.createRadialGradient(this.x, this.y, 0, this.x, this.y, (cr <= 0 ? 1 : cr));
       // g.addColorStop(this.stop, colorHexagon + (newo * opacityRect) + ')');

        conTwo.fillStyle = "#eb5d0b"
        conTwo.fill();
    }

    this.move = function () {
        WIDTH = window.innerWidth;
        HEIGHT = window.innerHeight;
        this.x += (this.rt / this.hl) * this.dx;
        this.y += (this.rt / this.hl) * this.dy;
        if (this.x > WIDTH || this.x < 0) this.dx *= -1;
        if (this.y > HEIGHT || this.y < 0) this.dy *= -1;
    }

    this.getX = function () { return this.x; }
    this.getY = function () { return this.y; }
}


$(window).on('resize',function(){
    WIDTH = window.innerWidth;
    HEIGHT = window.innerHeight;
    $(canvas).attr('width', WIDTH).attr('height', HEIGHT);
    $(canvasOne).attr('width', WIDTH).attr('height', HEIGHT);
    $(canvasTwo).attr('width', WIDTH).attr('height', HEIGHT);
});

})(jQuery);


