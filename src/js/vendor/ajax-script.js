(function($){

    var pageNumber = 1; //Posts will start from first page
    var content = $('#ajax-loop');
    var loading = true;
    var win = $(window);
    var viewportQuery = window.matchMedia('(min-width: 1024px)');
    viewportQuery.addListener(blogClick);

    function blogClick(viewportQuery) {

        if (viewportQuery.matches) {

            var blogPostLink = $('.blog-post-link');

            blogPostLink.on('click',function(e){
                    e.preventDefault();
                    var that = $(this);
                    var tileClicked = that.attr('href');


                     TweenMax.to($('body'), 0.7,
                        {
                            autoAlpha:0,
                            ease:Power4.easeInOut,
                            onComplete:function() {
                                window.location = tileClicked;
                            }
                        }

                    )

                });

        }else {
            TweenMax.to($('body'),0.4, 
                {
                    autoAlpha:1, 
                    ease:Power4.easeInOut
                }
            )
        }

    };

    function filterData() {
        var dataColors = ['greyColor', 'purpleColor', 'greenColor','orangeColor'];
        var counterColors = 0;

        $('.category-list li').each(function(){
            var that = $(this);
            that.attr('data-colors',dataColors[counterColors]);
            counterColors++;
        });
    }


    function showBlog() {

        $('.title-blog-tile').not(":first").hide();
        $('.category-list li').filter(":first").addClass('active-title');
    }

    showColor = function() {
        var that = $(this);
        var targetData = that.data('colors');

        var target = $('#ajax-loop').find('.title-blog-tile').filter(function() {
            return $(this).data('colors') === targetData;
        });

        if($(that).hasClass('active-title')) {
            return;
        }

        $('.title-blog-tile').hide();
        target.fadeIn('fast');

        $('.category-list li').removeClass('active-title');
        that.addClass('active-title');

        if($(that).hasClass('active-title')) {
            return;
        }

        
    }


    /*function resizePost() {
        var blogPost = $('.blog-post');
        $('.blog-post-content').css({
            'height': blogPost.width()
        });

        $('.blog-post-content').css({
            'width': blogPost.width()
        });
    }*/

    function mixItUp() {

       var dotcount = 0;
       $('.blog-post.mix').each(function(){
            $(this).attr('data-order', dotcount);
            dotcount=dotcount+1;
       });

          $('#ajax-loop').mixItUp({

            load: {
                sort: 'order:asc'
            },

            animation: {
                effectsIn: 'fade translateY(-100%)',
                duration: 700 /* 600 */
            },

            selectors: {
                target: '.mix',
                filter: '.filter-btn',
                sort: '.sort-btn'
            },

             callbacks: {
                onMixEnd: function(state){
                   // resizePost();
                }
             }

          });

    }

    

    

   function swapHomeBtn() {
        var header = $('.home-header');
        var backLink = header.find('.back-home');

        backLink.detach().prependTo(header);

    }


    function load_posts() {
        $.ajax({

            url:baseUrl + "/loop-ajax.php",
            type:"GET",
            dataType:"html",
            data: {GETposts : postsCount, GETpage : pageNumber},

            beforeSend:function() {
                content.append('<div style="text-align:center; display:block;" id="loading"><img src="' + baseUrl + '/gfx/preloader.gif"></div>');
            },

            success: function(data) {
                    var tekst = data.replace(/\s+/g, ''); // Aby uniknac wstawiania spacji tworzymy te zmienna
                    if (tekst.length) {

                        content.append(data);
                        loading = false; //Zakonczylo sie ladowanie postow
                        $("#loading").remove();

                    } else {
                        $("#loading").remove();
                        console.log(data);
                        content.append('<div style="text-align:center;">New stories are coming soon!</div>')
                    }

                    mixItUp();
                    //resizePost();
                    blogClick(viewportQuery);
                    swapHomeBtn();
                },

                error: function() {
                    console.log('Something went wrong with Ajax!');

            }

        });

    }

    
    win.on('scroll', function(){
        if ( !loading && ( win.scrollTop() + win.height() ) > ( content.scrollTop() + content.height() + content.offset().top ) ) {
            loading = true;
            pageNumber++;
            load_posts();
        };

        //var centerPoint = ($(window).height() - $('.footer-blog').outerHeight()) / 2;

        if(win.scrollTop() > 30) {
            TweenMax.to('.footer-blog', 0.6, 

             {
                bottom:0,
                ease:Power4.easeInOut
             }

            )
        }else if(win.scrollTop() < 30) {
            TweenMax.to('.footer-blog', 0.6, 

             {
               bottom:'-100%',
               ease:Power1.easeInOut
             }

            )
        }

    });


    $(function(){
        
        //resizePost();
        var colorsBTN = ['ipaye-blog', 'accountancy-blog', 'spi-blog'];
        var countClass = 0;
        var blogBTN = $('.category-list li').not(':first');

        blogBTN.each(function(index,item){

            $(item).addClass(colorsBTN[countClass]);
            countClass++;

        });

        filterData();
        showBlog();
        $('.category-list li').on('click', showColor);

    });

    $(window).on('load', function(){
        load_posts();
        swapHomeBtn();
        //mixItUp();
    });

    $(window).on('resize', function(){
        
            
    });


})(jQuery);